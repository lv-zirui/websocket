const port = '8000'
const base_url = 'localhost';
const ws_url = `ws://${base_url}:${port}`
function useWebSocket(handleMessage) {
  const ws = new WebSocket(ws_url);
  function init() {
    btnEvent();
  }
  // 给ws绑定事件
  function btnEvent() {
    ws.addEventListener('open', handleOpen, false);
    ws.addEventListener('close', handleClose, false);
    ws.addEventListener('error', handleError, false);
    ws.addEventListener('message', handleMessage, false)
  }
  const handleOpen = (e) => {
    console.log('open',e);
  }
  const handleClose = (e) => {
    console.log('close',e);
  }
  const handleError = (e) => {
    console.log('error',e);
  }
  init();
  return ws
}

export default useWebSocket;