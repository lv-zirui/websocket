// 封装一个时间处理函数

function useDateText(value) {
  if(!value) {
    alert('没有时间戳');
    return
  }
  // 如有有时间戳
  let date = new Date(value);
  // 年
  let y = date.getFullYear();
  // 月
  let m = date.getMonth() + 1;
  // 日
  let d = date.getDate();
  // 时分秒
  let h = date.getHours();
  let mi = date.getMinutes();
  let s = date.getSeconds();
  if(m<10) m = '0' + m;
  if(d<10) d = '0' + d;
  if(h<10) h = '0' + h;
  if(mi<10) mi = '0' + mi;
  if(s<10) s = '0' + s;
  return y + '-' + m + '-' + d + ' ' + h + ':' + mi + ':' + s;
}

export default useDateText;